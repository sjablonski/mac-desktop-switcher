//
//  AppDelegate.swift
//  RandomDesktop
//
//  Created by Tom Marks on 12/05/2015.
//  Copyright (c) 2015 UniKey. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {



    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // Insert code here to initialize your application
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }


}

