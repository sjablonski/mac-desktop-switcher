//
//  ViewController.swift
//  RandomDesktop
//
//  Created by Tom Marks on 12/05/2015.
//  Copyright (c) 2015 UniKey. All rights reserved.
//

import Cocoa

class ViewController: NSViewController, NSTextFieldDelegate {
//**************************************************************************************************
    // MARK: - IBOutlets
    
    @IBOutlet weak var amountOfTimeLabel: NSTextField!
    @IBOutlet weak var subreddit: NSTextField!
    @IBOutlet weak var spinner: NSProgressIndicator!
    @IBOutlet weak var timeTextField: NSTextField!
    @IBOutlet weak var updatingButton: NSButton!
    
//**************************************************************************************************
    // MARK: - Properties
    
    var flipflopImage = true
    var subredditText:String?
    var timeRemainingTimer:NSTimer?
    var updateTime:NSTimeInterval = 30
    var updateTimer:NSTimer?
    
//**************************************************************************************************
    // MARK: - View Controller Functions
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.spinner.stopAnimation(self)
        self.timeTextField.delegate = self
        self.subreddit.delegate = self
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear() {
        super.viewWillAppear()
        self.view.window?.title = " "
    }
    
//**************************************************************************************************
    // MARK: - IBActions
    
    @IBAction func update(sender: AnyObject) {
        
        self.resignFirstResponder()
        
        self.subredditText = self.subreddit.stringValue
        self.updatingButton.title = "Next"
        
        self.changeImage()
        self.updateTimer?.invalidate()
        self.updateTimer = NSTimer.scheduledTimerWithTimeInterval(self.updateTime, target: self,
            selector: Selector("changeImage"), userInfo: nil, repeats: true)
        
        self.timeRemainingTimer?.invalidate()
        self.timeRemainingTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self,
            selector: Selector("updateTimeUntilNextUpdate"), userInfo: nil, repeats: true)
        
    }
    
//**************************************************************************************************
    // MARK: - View Functions
    
    func updating() {
        self.updatingButton.enabled = self.updatingButton.enabled ? false : true
        
        if self.updatingButton.enabled {
            self.spinner.stopAnimation(self)
        } else {
            self.spinner.startAnimation(self)
        }
        
    }
    
//**************************************************************************************************
    // MARK: - Private Functions
    
    func changeImage() {
        
        self.imageURLsFromReddit(self.subredditText!) { (urls)->Void in
            self.downloadRandomImage(urls) { (imagePath:String)-> Void in
                println("\(imagePath)")
                self.setDesktop(imagePath)
                
                if let image = NSImage(contentsOfFile: imagePath) {
                    let imageView = NSImageView()
                    imageView.image = image
                        
                    NSApp.dockTile.contentView = imageView
                    NSApp.dockTile.display()
                } else {
                    NSApp.dockTile.contentView = nil
                    NSApp.dockTile.display()
                }
                
                
            }
        }
    }
    
    func downloadRandomImage(urls:[String], completed:(String)->()) {
        
        if urls.count == 0 {
            return
        }
        
        let index = Int(arc4random()) % urls.count
        
        let urlString = urls[index]
        var url = NSURL(string: urlString)
        
        println("\(url)")
        
        let request = NSURLRequest(URL: url!, cachePolicy: .UseProtocolCachePolicy,
            timeoutInterval: 30)
        self.updating()
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
            (response, data, error) -> Void in
            self.updating()
            let fileName = "image" + (self.flipflopImage ? "1" : "2") + ".jpg"
            self.flipflopImage = self.flipflopImage ? false : true
            
            var filePath = NSTemporaryDirectory().stringByAppendingPathComponent(fileName)
            
            if NSFileManager.defaultManager().createFileAtPath(filePath, contents: data,
                attributes: nil) {
                    println("saved")
            } else {
                println("failed")
            }
            
            completed(filePath)
        }
        
    }
    
    func imageURLsFromReddit(subreddit:String, completed:([String])->()) {
        
        let request = self.requestForSubreddit(subreddit)
        
        var error:NSError?
        var response:NSURLResponse?
        
        let data = NSURLConnection.sendSynchronousRequest(request, returningResponse: &response,
            error: &error)
        
        self.updating()
        
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue()) {
            (response, data, error) -> Void in
            self.updating()
            
            if error == nil {
                let json = NSJSONSerialization.JSONObjectWithData(data,
                    options: NSJSONReadingOptions.allZeros, error: nil) as! [String:AnyObject]
                
                let allData = json["data"] as! [String:AnyObject]
                
                let children = allData["children"] as! [[String:AnyObject]]
                
                var urlList = [] as [String]
                
                for dict in children {
                    let  finalData = dict["data"] as! [String:AnyObject]
                    
                    var url = finalData["url"] as! String
                    
                    if url.lowercaseString.rangeOfString("reddit") == nil {
                        urlList.append(url)
                    }
                }
                
                completed(urlList)
            }
        }
        
    }
    
    func localizedStringFromTimeIntervale(timeInterval:NSTimeInterval, dateComponentsUnitStyle
        unitStyle:NSDateComponentsFormatterUnitsStyle)->String? {
            let comps = NSDateComponents()
            comps.second = Int(timeInterval) % 60
            
            let minutes = timeInterval / 60
            let hours = minutes / 60
            let days = hours / 24
            
            comps.minute = Int(minutes) % 60
            comps.hour = Int(hours) % 24
            comps.day = Int(days)
            
            return NSDateComponentsFormatter.localizedStringFromDateComponents(comps,
                unitsStyle: unitStyle)
    }
    
    func requestForSubreddit(subreddit:String)->NSURLRequest {
        
        let url = String(format: "https://www.reddit.com/r/%@/.json", arguments: [subreddit])
        
        let request = NSURLRequest(URL: NSURL(string: url)!, cachePolicy: .UseProtocolCachePolicy,
            timeoutInterval: 30)
        
        return request
    }
    
    func setDesktop(filePath: String) {
        
        let url = NSURL(string: "file://localhost" + filePath)
        
        if let screens = NSScreen.screens() as? [NSScreen] {
            
            for screen in screens {
                var error: NSError?
                
                let screenOptions =
                [NSWorkspaceDesktopImageScalingKey :
                    NSNumber(integer: Int(NSImageScaling.ImageScaleNone.rawValue))
                    ,NSWorkspaceDesktopImageAllowClippingKey:NSNumber(bool: false),
                    NSWorkspaceDesktopImageFillColorKey:NSColor.blackColor()]
                
                NSWorkspace.sharedWorkspace().setDesktopImageURL(url!, forScreen: screen,
                    options: screenOptions as [NSObject : AnyObject], error: &error)
                
                if (error != nil) {
                    println("\(error)")
                    break
                }
                
            }
            
        }
        
    }
    
    func updateTimeUntilNextUpdate() {
        
        if let fireDate = self.updateTimer?.fireDate {
            let secondsBetween = fireDate.timeIntervalSinceNow
            
            let timeRemaining = self.localizedStringFromTimeIntervale(secondsBetween,
                dateComponentsUnitStyle: .Positional) ?? ""
            
            self.view.window?.title =
                "Time Until Next Update: " + timeRemaining
        }
    }
    
//**************************************************************************************************
    // MARK: - NSTextFieldDelegate Delegate
    
    override func controlTextDidChange(obj: NSNotification) {
        
        if let textField = obj.object as? NSTextField {
            if textField == self.timeTextField {
                
                if (self.timeTextField.stringValue as NSString).rangeOfCharacterFromSet(
                    NSCharacterSet.decimalDigitCharacterSet()).location == NSNotFound {
                        
                        self.amountOfTimeLabel.stringValue = "Fail. Use numbers only."
                        self.updateTime = 30
                        
                } else {
                    
                    self.updateTime =
                        NSTimeInterval((self.timeTextField.stringValue as NSString).integerValue)
                    
                    self.amountOfTimeLabel.stringValue =
                        self.localizedStringFromTimeIntervale(self.updateTime,
                            dateComponentsUnitStyle: .Full) ?? ""
                }
                
            }
            
            self.updatingButton.title = "Update"
            
        }
    }
    
}

